/* Main class that initializes an ArrrayList of Items.
 * It passes the arraylist to BatchOfItems class to generate the Batch
 * Calls the method from BatchOfItems that generates the Shipment Report.
 */

package com.retail.core;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
    	  GetItemFactory getItemFactory = new GetItemFactory();
    	  ArrayList<Item> list = new ArrayList<Item>();
    	  Item item;
    	  item = getItemFactory.getItem("AIR");
    	  item.setUpc(567321101987l);
      	  item.setDescription("CD - PinkFloyd, Dark Side of the Moon");
      	  item.setPrice(19.99);
      	  item.setWeight(0.58);
      	  list.add(item);
      	 
      	 item = getItemFactory.getItem("GROUND");
      	 item.setUpc(567321101986l);
    	 item.setDescription("CD - Beatles, Abbey Road");
         item.setPrice(17.99);
    	 item.setWeight(0.61);
    	 list.add(item);
        
    	 item = getItemFactory.getItem("AIR");
         item.setUpc(567321101985l);
     	 item.setDescription("CD - Queen, A Night at the Opera");
     	 item.setPrice(20.49);
     	 item.setWeight(0.55);
     	 list.add(item);
     	  
     	 item = getItemFactory.getItem("GROUND");

      	 item.setUpc(567321101984l);
       	 item.setDescription("CD - Michael Jackson, Thriller");
      	 item.setPrice(23.88);
      	 item.setWeight(0.50);
      	 list.add(item);
      	 
      	 item = getItemFactory.getItem("AIR");
      	 
       	 item.setUpc(467321101899l);
     	 item.setDescription("iphone - Waterproof case");
     	 item.setPrice(9.75);
     	 item.setWeight(0.73);
     	 list.add(item);
     	 item = getItemFactory.getItem("GROUND");
     	 item.setUpc(477321101878l);
      	 item.setDescription("iPhone - Headphones");
      	 item.setPrice(17.25);
      	 item.setWeight(3.21);
      	 list.add(item);
      	
      	item = getItemFactory.getItem("RAIL");
  	    item.setUpc(312321101516l);
        item.setDescription("Hot Tub");
        item.setPrice(9899.99);
    	item.setWeight(793.41);
    	list.add(item);
      	
    	
    	item = getItemFactory.getItem("RAIL");
  	    item.setUpc(322322202488l);
        item.setDescription("HeavyMac Laptop");
        item.setPrice(4555.79);
    	item.setWeight(4.08);
    	list.add(item);
      	
    	BatchOfItems batch1 =new BatchOfItems(list);
      	 batch1.generateBatchReport();
    	
    }
}
