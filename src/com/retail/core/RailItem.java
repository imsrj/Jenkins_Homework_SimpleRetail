package com.retail.core;

public class RailItem extends Item{
	public RailItem() {
		super("RAIL");
		// TODO Auto-generated constructor stub
	}
	@Override
	public int compareTo(Object compareItem) {
		// TODO Auto-generated method stub
		Item compare = (Item)compareItem;
		return new Long(this.getUpc()).compareTo(new Long(compare.getUpc()));
		
	}

	@Override
	public double getShippingCost() {
		double cost;
		if(this.weight<5)
			cost = 5;
		else
			cost =10;
		// TODO Auto-generated method stub
		return cost;
	}

}
