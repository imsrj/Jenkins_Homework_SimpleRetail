// Abstract class Item, contains appropriate getters and setters for the fields as per the design requirements
 
// Implements Comparable Interface
package com.retail.core;


public abstract class Item implements Comparable {
	 long upc;
     String description;
     double price;
     double weight;
     private String shippingMethod;
     
     
     public Item(String method) {
    	 shippingMethod = method;
    	 
    	
     }
     
     public abstract double getShippingCost();

	public long getUpc() {
		return upc;
	}

	public void setUpc(long upc) {
		this.upc = upc;
	}

	public String getDescription() {
		return description;
	}
	public String getshippingMethod() {
		return shippingMethod;
	}

	public void setDescription(String description) throws NullPointerException{
		if(description.equals(null))
			throw new NullPointerException("Description cannot be empty");
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) throws IllegalArgumentException {
		if(weight  < 0)
			throw new IllegalArgumentException("Weight cannot be negative");
		this.weight = weight;
	}
     
}
