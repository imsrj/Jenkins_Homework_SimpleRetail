//  Factory class where Items with appropriate Shipping method gets instantiated.


package com.retail.core;

public class GetItemFactory {
      public Item getItem(String itemType) {
    	  if(itemType == null) {
    		  return null;
    	  }
    	  if(itemType.equalsIgnoreCase("AIR")) {
    		  return new AirItem();
    	  }
    	  
    	  else if(itemType.equalsIgnoreCase("GROUND")) {
    		  return new GroundItem();
    	  }
    	  else if(itemType.equalsIgnoreCase("RAIL")) {
    		  return new RailItem();
    	  }
    	  
    	  return null;
      }
}
