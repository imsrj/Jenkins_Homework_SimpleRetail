package com.retail.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.retail.core.GetItemFactory;
import com.retail.core.Item;

public class RailItemTest {

Item item1;
	
	@Before
	public void setUp() {
		
		GetItemFactory getItemFactory = new GetItemFactory();
  	    //item1 = getItemFactory.getItem("AIR");
  	    item1 = getItemFactory.getItem("RAIL");
  	}
	    

	@Test
	public void testRailShippingCostPositive() {
		   
	  item1.setUpc(567321101987l);
  	  item1.setDescription("CD - PinkFloyd, Dark Side of the Moon");
  	  item1.setPrice(19.99);
  	  item1.setWeight(5);
  	  assertEquals(10, item1.getShippingCost(), 0.0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRailShippingCostNegative() {
		   
	  item1.setUpc(567321101987l);
  	  item1.setDescription("CD - PinkFloyd, Dark Side of the Moon");
  	  item1.setPrice(19.99);
  	  item1.setWeight(-5);
  	 
	}
	
	@Test(expected = NullPointerException.class)
	  public void testRailShippingCostNull() {
		   
		  item1.setUpc(567321101987l);
	  	  item1.setDescription(null);
	  	  item1.setPrice(19.99);
	  	  item1.setWeight(10);
	   
	}



}
